#!/bin/bash
# helper to move games inside backup snapshots when moving them
# between different steam library disks (so that backup does not keep two copies of them)

RS="localhost_root${HOME}/.local/share/Steam/steamapps/common"
DS="localhost_data/mnt/data/games/steamapps/common"

for F in {daily.{0,1,2,3,4,5},weekly.{0,1,2}}; do
	for G in "ARK"; do
		## ^ Update list of games here
		## choose which direction to move:

		## from root home steam lib to second data disk
		#mv "${F}/${DS}/${G}" "${F}/${RS}/";

		## from second data disk to root home steam lib
		mv "${F}/${DS}/${G}" "${F}/${RS}/";
	done
done
