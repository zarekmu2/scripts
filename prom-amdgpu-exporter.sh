CARD=${CARD:-card0}
CARDMON=${CARDMON:-hwmon1}

# time reporting here is expensive, so do not do it
#
# in my local machine overall collection takes ~50ms
# but time reporting (executing the date/cut/echo)
# adds extra 10-16 msec ... just to print 50
# so unless needed for e.g. debugging, not worth 20-30% slowdown
REPORT_TIME=0

DEVICE="/sys/class/drm/$CARD/device"
HWMON="$DEVICE/hwmon/${CARDMON}"

if [[ "1" = "$REPORT_TIME" ]]; then
    MSEC_START=$(date +%s%N | cut -b1-13)
fi

# PERCENT
GPU_BUSY=$(cat $DEVICE/gpu_busy_percent)
# 8 or 16  # so far
PCIE_LANES=$(sed -nr 's|.*x([0-9]+)( \*)?$|\1|p' < $DEVICE/pp_dpm_pcie)
# 2.5 or 8.0   # so far
PCIE_SPEED_GTS=$(sed -nr "s|^([0-9.]+) .*|\1|p" < $DEVICE/current_link_speed)
# bytes
MEM_TOTAL=$(cat $DEVICE/mem_info_vram_total)
# bytes
MEM_USED=$(cat $DEVICE/mem_info_vram_used)
# bytes
MEM_GTT_TOTAL=$(cat $DEVICE/mem_info_gtt_total)
# bytes
MEM_GTT_USED=$(cat $DEVICE/mem_info_gtt_used)

# 1/1000 of Celsius? (42*C current in my case)
TEMP1=$(cat $HWMON/temp1_input)
# 1/1000 of Celsius? (94*C max in my case)
TEMP1_CRIT=$(cat $HWMON/temp1_crit)
# 0/1 (bool)
FAN1_ENABLE=$(cat $HWMON/fan1_enable)
# number (int?)
FAN1_SPEED=$(cat $HWMON/fan1_input)
# number (int?)
FAN1_MAXSPEED=$(cat $HWMON/fan1_max)


cat <<EOF
# HELP amd_gpu_busy GPU Utilization
# TYPE amd_gpu_busy gauge
amd_gpu_busy $GPU_BUSY
# HELP amd_gpu_pcie_lanes PCIe Lane count
# TYPE amd_gpu_pcie_lanes gauge
amd_gpu_pcie_lanes $PCIE_LANES
# HELP amd_gpu_pcie_speed_gts PCIe Speed in GT/s
# TYPE amd_gpu_pcie_speed_gts gauge
amd_gpu_pcie_speed_gts $PCIE_SPEED_GTS
# HELP amd_gpu_mem_vram_total_bytes Total amount of memory for VRAM in bytes
# TYPE amd_gpu_mem_vram_total_bytes gauge
amd_gpu_mem_vram_total_bytes $MEM_TOTAL
# HELP amd_gpu_mem_vram_used_bytes Amount of memory used for VRAM in bytes
# TYPE amd_gpu_mem_vram_used_bytes gauge
amd_gpu_mem_vram_used_bytes $MEM_USED
# HELP amd_gpu_mem_gtt_total_bytes Total amount of memory for GTT in bytes (likely same as vram)
# TYPE amd_gpu_mem_gtt_total_bytes gauge
amd_gpu_mem_gtt_total_bytes $MEM_GTT_TOTAL
# HELP amd_gpu_mem_gtt_used_bytes Amount of memory used for GTT in bytes
# TYPE amd_gpu_mem_gtt_used_bytes gauge
amd_gpu_mem_gtt_used_bytes $MEM_GTT_USED
# HELP amd_gpu_temp_celsius Current temperature of GPU
# TYPE amd_gpu_temp_celsius gauge
amd_gpu_temp_celsius $TEMP1
# HELP amd_gpu_temp_crit_celsius Temperature consider critical for GPU
# TYPE amd_gpu_temp_crit_celsius gauge
amd_gpu_temp_crit_celsius $TEMP1_CRIT
# HELP amd_gpu_fan_enabled State GPU Fan (disabled=0, enabled=1)
# TYPE amd_gpu_fan_enabled gauge
amd_gpu_fan_enabled $FAN1_ENABLE
# HELP amd_gpu_fan_speed GPU Fan speed (rpm?)
# TYPE amd_gpu_fan_speed gauge
amd_gpu_fan_speed $FAN1_SPEED
# HELP amd_gpu_fan_speed_max GPU Fan max speed (rpm?)
# TYPE amd_gpu_fan_speed_max gauge
amd_gpu_fan_speed_max $FAN1_MAXSPEED
EOF
if [[ "1" = "$REPORT_TIME" ]]; then
    MSEC_END=$(date +%s%N | cut -b1-13)
    echo "# HELP amd_gpu_completion_msec How long it took to gather the amdgpu metrics"
    echo "# TYPE amd_gpu_completion_msec gauge"
    echo "amd_gpu_completion_msec $(( $MSEC_END - $MSEC_START ))"
fi
