#!/bin/bash

override=""
if [[ "$1" != "help" && "$1" != "-h" && "$1" != "--help" ]]; then
    override="$1"
fi

current=$(setxkbmap -query|sed -rn 's/layout: +(.*)/\1/p')

next=vok
[[ $next = $current ]] && next=cz
next=${override:-$next}
setxkbmap $next
echo "$(date): configuring $next" >> /tmp/qskbmap-${USER}.log
echo "layout: $next" | osd_cat -d1 -o100 -f "-*-fixed-*-*-*-*-*-140-*-*-*-*-*-*"
