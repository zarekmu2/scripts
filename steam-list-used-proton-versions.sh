#!/bin/bash

DEF_GAME_LIB="$HOME/.local/share/Steam"

if [[ "$1" == "--help" ]]; then
    echo "$0 [steam_game_lib...]"
    echo ""
    echo "Provide one or more paths to steam game libraries,"
    echo "meaning to dirs in which 'steamapps' exists."
    echo ""
    echo "default game lib: '$DEF_GAME_LIB'"
    exit 0
fi
if [[ "$1" == "--debug" ]]; then
    shift
    set -x
fi

declare -a GAME_LIBS
while [[ "$#" -gt 0 ]]; do
    GAME_LIBS=("${GAME_LIBS[@]}" "$1")
    shift
done

if [[ "${#GAME_LIBS[@]}" -lt 1 ]]; then
    GAME_LIBS=("$DEF_GAME_LIB")
fi

for D in "${GAME_LIBS[@]}"; do
    cd "$D/steamapps/compatdata";
    for F in $(ls -1 */version); do
        echo "$(cat $F)  ${F%/*}  $D";
    done;
done | sort -h
