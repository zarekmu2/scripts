#!/bin/bash

print_help() {
cat <<EOF
expected usage when fully implemented:
> wifi-weak-switch [-i <ifc>] <minLinkQuality> <ssidA> <ssidB> ...

this script call can be then added as cron entry (frequency as you prefer)
and when trigger it will check if current wifi is in the specified ssid list
if so, it will check if current connection quality is greater or same as minimal link quality
if quality is below, it will scan networks and select the one from list of ssid's which has highest quality

So this script when periodically called should automatically reconnect you to strongest of allowed ssid/APs.
Operation can be limited to specific interface using '-i wlan0' opt as first parameters.


ATM it's work in progress:
- only parse current sssid, and quality link and log those
EOF
}

IFC=""
MINQUAL=""
declare -a KNOWN_SSID

while [[ "$#" -gt 0 ]]; do
    if [[ "$1" == "-i" ]]; then
        shift
        IFC="$1"
    elif [[ -z "$MINQUAL" ]]; then
        MINQUAL="$1"
    else
        KNOWN_SSID=(${KNOWN_SSID[@]} "$1")
    fi
    shift
done

if [[ "$MINQUAL" == "--help" || "$MINQUAL" == "help" ]]; then
    print_help
    exit 0
fi
if [[ -z "$MINQUAL" || -z "${KNOWN_SSID[*]}" ]]; then
    print_help

    echo ""
    echo "IFC: $IFC"
    echo "MINQUAL: $MINQUAL"
    echo "KNOWN_SSID: ${KNOWN_SSID[@]}"
    echo "ERROR: Wrong params."
    exit 1
fi

WINFO=$(iwconfig $IFC 2>&1)
SSID=$(sed -nr 's/^.*\s802.11\s+ESSID:"(.*)".*$/\1/p'<<<"$WINFO")
LINKQ=$(sed -nr 's|^.*Link Quality=([0-9]+)/.*$|\1|p'<<<"$WINFO")
echo "CURRENT SSID: $SSID"
echo "CURRENT QUALITY: $LINKQ"

if [[ "$LINKQ" -ge "$MINQUAL" ]]; then
    echo "Quality of $SSID is $LINKQ which seems enough (>=$MINQUAL)."
    exit 0
fi
BEST_SSID="$SSID"
BEST_QUALITY="$LINKQ"
echo "searching for better network ..."
while read FOUND; do
    FOUND_SSID=$(sed -nr 's/^\*?:(.*):([0-9]+)$/\1/p' <<<"$FOUND")
    FOUND_QUALITY=$(sed -nr 's/^\*?:(.*):([0-9]+)$/\2/p' <<<"$FOUND")
    if [[ "$FOUND_QUALITY" -gt "$BEST_QUALITY" ]]; then
        if grep -q -E "(^${FOUND_SSID} )| ${FOUND_SSID} |( ${FOUND_SSID}$)" <<<"${KNOWN_SSID[*]}"; then
            BEST_SSID="${FOUND_SSID}"
            BEST_QUALITY="${FOUND_QUALITY}"
        fi
    fi
done < <(nmcli -t -f IN-USE,SSID,SIGNAL dev wifi list ifname wlan0 --rescan yes)

if [[ "${BEST_QUALITY}" == "${SSID}" ]]; then
    echo "Nothing better then current $SSID was found."
    exit 0
fi

echo "Switching to best: ${BEST_SSID} @ ${BEST_QUALITY}"
nmcli con up id "${BEST_SSID}"
