#!/bin/bash


### Usage:
###  git verify [-b -|verify-branch-name] [-q|--quiet] [--all] [-m|--ignore-missing] [-np|--no-push] [-pr|--preview] [-r|--review] [branchA, branchB, ...]
###
### You can use this to automate mass-branch-merging of
### multiple independant/separate patch-branches to one superior,
### for example to verify multiple changes together.
### This can rebuild 'verify' branch from those previously used,
### on top of new base (master) branch and opt. add more patch branches.
###
### After merging those, it can automatically push them to your
### own git remote (personal mirror). This requires more configuration
### see bellow for auto-push related config.
###
### Example:
###   Have your git repo cloned somewhere.
###   Have remote named 'my' registered there.
###   Have few patch branches with (non-conflicting) changes.
###   Opt. have your branch with personal changes there
###   (default is empty which means no 'always merge' branches).
###
### go to your repo-working-copy-dir
### execute:
###
###  git verify patch1 patch-extra
###  # (re)create 'verify'
###  # - drops verify, creates again from master (base branch)
###  # merge personal branch:
###  # - merge in 'personal' if enabled by 'always merge' opt.
###  # merge patch branches as provided: 'patch1', 'patch-extra'
###
###  git verify
###  # lists what personal/patch branches are contained in the 'verify' branch
###
###  git verify --all
###  # recreate 'verify' branch:
###  # - destroy 'verify' branch
###  # - checkout branch 'master' as 'verify'
###  # - opt. merge you personal branch there
###  # - in order, merge all previously merged branches (patch1, patch-extra)
###
###  git verify --all additional-extra
###  # recreate, merge those already contained (see above)
###  # + merge in 'additional-extra' branch
###
### To skip missing branches (whatever if specified explicitely or via --all)
### use -m (--ignore-missing) (or verify.branch.ignoremissing=true).
###
### This refuses to continue if your working-space or index (staged) contain any modifications.
###
### Three basic things are configurable (via cli-opt, env-var or git-config key)
###  - base branch (defaults to 'master')
###     * on top of which branch should all other (personal, patches) branches be merged
###     * git config verify.branch.base
###     * env var BASE_BR
###  - verify branch (defaults to 'verify')
###     * name of target branch to be (re)created (will be dropped in some cases!)
###       or from which the list of 'previously merged' branches should be obtained/listed
###     * git config verify.branch.target
###     * env var VERIFY_BR
###     * -b <name> cli option, has to be specified first before any other arg,
###       when dash value is provided (-b -)
###       it means  recreate currently active branch!
###  - always merge branch(es) (empty by default)
###     * aka personal branches, string with space delimited branch names
###     * git config verify.branch.alwaysmerge (e.g. 'personal')
###     * env var GIT_VERIFY_ALWAYS_MERGE
###     Be aware that these personal branches will always be rebased
###     on top of master (base branch) before merging into verify,
###     and they are merged first before the rest of patch branches.
###
### For auto-pushing you need to configure also:
###  - name of the remote where to auto-push the verify branch (empty)
###     * git config verify.push.remote (e.g. 'my')
###  - name of remote branch where to auto-push the verify branch (empty)
###     * git config verify.push.branch (e.g. 'verify')
###  - which branches to also always-auto-push (empty)
###     * git config verify.push.always (e.g. 'personal')
###  - to temporarily skip pushing (when it's configured)
###     * -np || --no-push CLI opt can be used
###
### If you don't like having verify (target) branch active
### after this command finishes, and instead You want to be
### returned to whatever branch was active before git-verify invocation:
###  - git config verify.switchback true
### (default=false, means stay at the verify branch after finishing)
###
### To automatically send all the commits from patch-branches
### on gerrit for review, you can use
###  -r || --review [--all] [branches-to-upload ...]
### which will instead of rebuilding/pushing the verify branch
### invoke 'git review --yes' on every branch specified,
### or contained in the 'verify' (in case of --all)
### excluding the personal 'always_merge' branches.
### If you want to force same 'topic' for all, set FORCE_REVIEW_TOPIC env var.
###
### To get preview of branches before uploading to review use
###  -pr || --preview [--all | branchs to upload]
###
### To reduce some of the output to minimum use:
###  -q || --quiet
### Although it is implemented right now just for listing branches merged in verify branch.
### (the no param specified case)
###

if [[ "$1" == "help" || "$1" == "-h" || "$1" == "--help" ]]; then
    # show the above comment as help
    sed -n 's/^###/ /p' $0
    exit
fi

if ! git rev-parse --git-dir &> /dev/null; then
    echo "Does not looks that $(pwd) is a git repo!" >&2
    exit 1
fi



# config
BASE_BR="${BASE_BR:-$(git config verify.branch.base 2> /dev/null)}"
BASE_BR="${BASE_BR:-master}"

VERIFY_BR="${VERIFY_BR:-$(git config verify.branch.target 2> /dev/null)}"
VERIFY_BR="${VERIFY_BR:-verify}"

ALWAYS_MERGE="${GIT_VERIFY_ALWAYS_MERGE:-$(git config verify.branch.alwaysmerge 2> /dev/null)}"
IGNORE_MISSING="${IGNORE_MISSING:-$(git config verify.branch.ignoremissing 2> /dev/null)}"
IGNORE_MISSING="${IGNORE_MISSING:-false}"

AUTOPUSH_REMOTE="$(git config verify.push.remote 2> /dev/null)"
AUTOPUSH_BR="$(git config verify.push.branch 2> /dev/null)"
AUTOPUSH_ALWAYS="$(git config verify.push.always 2> /dev/null)"

SWITCH_BACK="$(git config --bool verify.switchback 2> /dev/null)"

REBASE_BR="${REBASE_BR:-}"

FILES_TO_CLEAN=""




set -o errexit

# helpers

list_merged() {
    git log $VERIFY_BR --merges \
        | sed -n "s/.*branch '\(.*\)' into ${VERIFY_BR}.*/\1/p" | tac;
}

test_branch() {
    if git branch | grep -q "$1" && git log -1 "$1" -- &>/dev/null; then
        return
    fi
    if [[ "$IGNORE_MISSING" = "true" ]]; then
        echo "WARNING: Branch $1 not found."
        return 1
    fi
}
rebase_branch() {
    local BR="$1"
    git checkout "$BR"
    git rebase $BASE_BR
    if [[ $? != 0 ]]; then
        echo "Branch $BR conflicts with $BASE_BR!"
        git rebase --abort
        exit 1
    fi
}

active_branch() {
    git branch | sed -nr 's/\* (.+)$/\1/p'
}

finish() {
    echo "==[ DONE ]=="
    if [[ ! -z "$FILES_TO_CLEAN" ]]; then
        rm -rf "$(ls $FILES_TO_CLEAN)"
    fi
    if [[ "$SWITCH_BACK" = "true" ]]; then
        git checkout $ORIG_BRANCH
    fi
    exit ${1:-0}
}



# parse the arguments

PREV_BRANCHES=""
ARGS_BRANCHES=""
PUSH_ALLOWED=y
UPDATE_PREVIEW=n
UPDATE_REVIEW=n
QUIET=false

while [[ $# -gt 0 ]]; do
    case "$1" in
        -b|--branch)
            shift
            VERIFY_BR="$1"
            if [[ "$VERIFY_BR" = "-" ]]; then
                VERIFY_BR=$(active_branch)
            fi
            ;;
        -np|--no-push)
            PUSH_ALLOWED=n
            ;;
        -pr|--preview)
            UPDATE_PREVIEW=y
            ;;
        -r|--review)
            UPDATE_PREVIEW=y
            UPDATE_REVIEW=y
            ;;
        -m|--ignore-missing)
            IGNORE_MISSING=true
            ;;
        -a|--all)
            PREV_BRANCHES="$(list_merged)"
            ;;
        -q|--quiet)
            QUIET=true
            ;;
        *)
            ARGS_BRANCHES="$ARGS_BRANCHES $1"
            ;;
    esac
    shift
done



# if nothing specified, just list the verify branch

$QUIET || echo "==/ $VERIFY_BR /=="

if [[ -z "$PREV_BRANCHES" && -z "$ARGS_BRANCHES" ]]; then
    $QUIET || echo "==[ No branches specified ... listing last merges ]=="
    IGNORE_MISSING=true
    if test_branch "$VERIFY_BR"; then
        $QUIET || echo ""
        BRANCHES="$(list_merged)"
        echo " " $BRANCHES # unquoted variable used to get the names on one line
    else
        echo "No already merged branches as there is no $VERIFY_BR branch"
    fi
    if $QUIET; then exit 0; fi
    echo ""
    echo "---- all existing branches ----"
    git branch --sort=creatordate
    echo ""
    for BR in $BRANCHES; do
        test_branch $BR
    done
    exit 0
fi



# Stuff which does have impact (rebase, push, review) follows
# so we do first check for clean state.

if [[ ! -z "$(git status --porcelain)" ]]; then
    echo "==[ Unclean working copy ]=="
    echo "Please first commit/stash/remove your local modifications.";
    echo ""
    git status
    exit 1
fi

ORIG_BRANCH="$(active_branch)"
BRANCHES="$PREV_BRANCHES $ARGS_BRANCHES"

echo $BRANCHES
WANTED_BRANCHES="$BRANCHES"
BRANCHES=""
for BR in $WANTED_BRANCHES; do
    any_missing=0
    if test_branch "$BR"; then
        BRANCHES="$BRANCHES $BR"
    else
        any_missing=1
    fi
    if [[ "$IGNORE_MISSING" != "true" && $any_missing = 1 ]]; then
        exit 128
    fi
done

if [[ "$UPDATE_PREVIEW" = "y" ]]; then
    FAILED_REVIEWS=""
    NOCHANGE_REVIEWS=""
    REVIEWLOG=$(mktemp)
    FILES_TO_CLEAN="$FILES_TO_CLEAN $REVIEWLOG"
    FORCE_REVIEW_TOPIC="${FORCE_REVIEW_TOPIC:-}"
    topic_opt=""
    if [[ -n "$FORCE_REVIEW_TOPIC" ]]; then topic_opt="-t"; fi
    for BR in $BRANCHES; do
        echo "==[ Uploading $BR ]=="

        git log ${BASE_BR}..${BR}

        if [[ "$UPDATE_REVIEW" = "y" ]]; then
            git checkout $BR
            git review --yes $topic_opt $FORCE_REVIEW_TOPIC | tee $REVIEWLOG
            rc="${PIPESTATUS[0]}"
            if [[ "$rc" != "0" ]]; then
                if grep -q '(no new changes)' $REVIEWLOG; then
                    NOCHANGE_REVIEWS="$NOCHANGE_REVIEWS $BR"
                else
                    FAILED_REVIEWS="$FAILED_REVIEWS $BR"
                fi
            else
                UPDATED_REVIEWS="$UPDATED_REVIEWS $BR"
            fi
        fi
        echo ""
    done
    echo "Updated reviews for branches: $UPDATED_REVIEWS"
    echo "Already up-to-date reviews for: $NOCHANGE_REVIEWS"
    if [[ ! -z "$FAILED_REVIEWS" ]]; then
        echo "Failed to upload branches: $FAILED_REVIEWS" >&2
        finish 1
    fi
    finish
fi

echo "==[ Going to rebase branches: ]=="

for BR in $ALWAYS_MERGE; do
    echo "* $BR"
    test_branch "$BR" || continue
    rebase_branch "$BR"
done
if [[ -n "$REBASE_BR" ]]; then
    [[ "$REBASE_BR" == "--all" ]] && REBASE_BR="$BRANCHES"
    for BR in $REBASE_BR; do
        echo "* $BR"
        test_branch "$BR" || continue
        rebase_branch "$BR"
    done
fi

echo ""

echo "==[ Going to merge branches: ]=="
set -e

git checkout --detach $BASE_BR

for BR in $ALWAYS_MERGE; do
    echo "==== $BR"
    git merge --ff-only $BR
done

for BR in $BRANCHES; do
    echo "==== $BR"
    if ! git merge --no-ff --commit --no-edit -m "Merge branch '$BR' into $VERIFY_BR" $BR; then
        if [[ "$(git config rerere.enabled)" == "true" && "$(git rerere status)" == "" && "$(git rerere remaining)" == "" ]]; then
            git add $(git status | sed -nr 's/^\s+both modified:\s*//p')
			echo "Merging based on previous solution to conflict ..."
            git commit --no-edit
        else
            echo "Unable to auto-resolve the conflict."
            echo "Enable git-rerere, resolve the conflict once by hand to record the solution and then rerun git-verify."
            exit 1
        fi
    fi
done

git branch -f $VERIFY_BR
git checkout $VERIFY_BR

echo "==[ Auto-push ]=="
if [[ "$PUSH_ALLOWED" != "y" ]]; then
    echo "Auto-push disabled."
elif [[ -z "$AUTOPUSH_REMOTE" ]]; then
    echo "No remote configured - skipping."
else
    if [[ -z "$AUTOPUSH_ALWAYS" ]]; then
        echo "No always-push branches configured - skipping."
    else
        for BR in $AUTOPUSH_ALWAYS; do
            echo "Always-Pushing branch: $AUTOPUSH_ALWAYS:"
            git push --force "$AUTOPUSH_REMOTE" "${BR}:${BR}"
        done
    fi

    if [[ -z "$AUTOPUSH_BR" ]]; then
        echo "No target verify branch configured - skipping."
    else
        echo "Pushing target branch: $AUTOPUSH_BR:"
        git push --force "$AUTOPUSH_REMOTE" "${VERIFY_BR}:${AUTOPUSH_BR}"
    fi
fi

finish
