#!/bin/bash

### == QSLocate ==
###
### Very dump but way faster alternative to slocate/mlocate.
###
### qslocate <needle-to-search-for>
###     - search for needle in the file database
### qslocate [path-prefix] <needle-to-search-for>
###     - search for needle in db, while filtering
###       the paths of results into specified path-prefix (directory)
###       (relative paths are ok here, as it gets canonicalized first)
### qslocate --update [--nice]
###     - update/create file database
###     - use --nice param for example in cron jobs
###       will decrease cpu and i/o priority to not affect system performance
###
### - just keep plaintext results of `find` in own file
### - use grep for searching for files
### - obviously this ignore any multi-user/permissions things, time information and such
### - but 99.9% of time i do not care/need any of that (as A LOT of people it seems)
### - and this does that job (in remaining 0.0..% of time it's ok to wait for full find ;))
### - benefits are speed (yay!) and well-known-daily-used grep arguments interface
### - inspired by https://jvns.ca/blog/2015/03/05/how-the-locate-command-works-and-lets-rewrite-it-in-one-minute/
###
### == Configuration ==
###
### Config variables can be set in env or in config file `$HOME/.config/qslocate`.
###
### DB
###     - string, path to file where to keep index
### SOURCES
###     - bash array, paths/directories to index files from
###     - e.g.  SOURCE=(/home/me /var/something /etc/)
###     - atm. one can try embedding `find` arguments here
###       e.g. SOURCES=("$HOME" "-maxdepth 3" "-type d")
###       it may work, i will keep that working but no 100% promise
###       (if i drop this "feature" i'll add alternative way)
###
### Copyright (c) 2020 Queria Sa-Tas. All rights reserved.
### The MIT License applies.

if [[ -z "$1" || "$1" == "--help" || "$1" == "help" ]]; then
	sed -n "s/^### \?//p" $0
	exit 0
fi

if [[ -f "$HOME/.config/qslocate" ]]; then
	source "$HOME/.config/qslocate"
fi
DB="${DB:-$HOME/.cache/qslocate.db}"
if [[ "${#SOURCES[@]}" == "0" ]]; then
	SOURCES=("$HOME")
fi

if [[ "$1" == "--update" ]]; then
    shift

    if [[ "$1" != "--locked" ]]; then
        flock -E 42 -n "$DB" $0 --update --locked "$@"
        rc=$?
		if [[ $rc == 42 ]]; then
			echo "ERROR: Update of $DB already in progress?" >&2
		fi
        exit $rc
    else
        shift
    fi

    if [[ "$1" == "--nice" ]]; then
        renice -n 10 -p $$;
        ionice -c 3 -p $$;
    fi

	echo "Updating database $DB ..."
    find ${SOURCES[@]} -print0 | xargs -0 readlink -f > "${DB}.tmp"
	#rc=$?
	#if [[ $rc == 0 ]]; then
	#	mv "${DB}.tmp" "${DB}"
	#	rc=$?
	#fi
	#if [[ $rc != 0 ]]; then
	#	echo "ERROR: see above why this FAILED" >&2
	#else
	#	echo "DONE"
	#fi
	#exit $rc
    mv "${DB}.tmp" "${DB}"
    echo "DONE"
    exit
fi

if [[ ! -e "$DB" ]]; then
	echo "ERROR: File database $DB does not exist yet!  (first run '$0 --update')" >&2
fi

if [[ -n "$2" ]]; then
    grep -i "^$(readlink -f "${1}")/.*${2}" "$DB"
else
    grep -i "$@" "$DB"
fi
