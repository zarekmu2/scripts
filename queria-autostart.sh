#!/bin/bash

LOG=$HOME/.cache/qs-autostart.log
truncate -s0 $LOG
echo "$(readlink -f $0): Logging in $LOG"
exec 1<&-
exec 2<&-
exec 1>>$LOG
exec 2>&1
date

xset dpms 0 0 0
xset m 1/1 0
[[ -e "$HOME/all" && ! -e "$HOME/all/tmp" ]] && (mkdir /tmp/ps_tmp && ln -s /tmp/ps_tmp "$HOME/all/tmp")

setxkbmap vok
echo "$(dirname "$(readlink -f "$0")")/kbmap.sh" vok
"$(dirname "$(readlink -f "$0")")/kbmap.sh" vok

[[ -e "$HOME/all/src/scripts/xinput-config" ]] && "$HOME/all/src/scripts/xinput-config"

runcond() {
    if which $1 &> /dev/null; then
        "$@" &> /dev/null &
    fi
}

urxvt256cd -f -o
#runcond xosd-sysmon
runcond qslock-auto
#runcond qsrun
#runcond nm-applet
#runcond compton -b
runcond cpu-switch.sh optim

if [[ "$XDG_CURRENT_DESKTOP" != "KDE" ]]; then
    systemctl start --user pulseaudio
    runcond dunst
    runcond xfce4-clipman
    feh --bg-center --bg-max $HOME/wallpaper.png
fi


xset s off
