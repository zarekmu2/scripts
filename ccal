#!/usr/bin/env python3

from datetime import date, timedelta
import sys
from textwrap import dedent


def doc():
    print(dedent("""
          ccal <days-count> [start-date]

          Calculate what date is X days after/before today (or given date).


          === Arguments ===

          days-count:   how many days to add/substract from start-date
                        positive or negative integer
                        e.g. 14 or -50
          start-date:   what date to count the days from in YYYY-MM-DD form
                        default is today
                        'today' or 'now' are accepted values with same meaning
                        e.g.  '2000-01-01' or 'now'


          === Examples ===
                ccal 14 2000-01-01    |=>   2000-01-15
                ccal -356 2000-01-01  |=>   1999-01-01
                ccal 356 2000-01-01   |=>   2000-12-31  # since it's leap year
          """))


def str2date(day_str):
    if day_str in ('', 'now', 'today'):
        return date.today()
    else:
        try:
            day_parts = [int(p) for p in day_str.split('-')]
            return date(day_parts[0], day_parts[1], day_parts[2])
        except IndexError:
            print('ERROR: invalid start-date input:  %s' % day_str)
            doc()
            sys.exit(1)


def main():
    if (len(sys.argv) < 2 or '--help' in sys.argv or 'help' in sys.argv):
        doc()
        sys.exit(0)

    print(sys.argv, len(sys.argv))

    offset = int(sys.argv[1])

    day_input = 'today' if len(sys.argv) < 3 else sys.argv[2]
    day = str2date(day_input)

    result = day + timedelta(days=offset)

    print('%s\t(= %s %s %s days)' % (
        result,
        day,
        '-' if offset < 0 else '+',
        abs(offset)
    ))


if __name__ == '__main__':
    main()
