#!/bin/bash

# samsung are morons, while i was happy with previous tv from them
# i wouldn't realize new ones are intentionally crippled when it gets to video codec support
# ... just search for 'samsung tv 2020 xvid', they kept all other formats but dropped this historically "most used" one
#
# thx to short tip https://www.avforums.com/threads/a-simple-trick-to-play-avi-xvid-divx-videos-on-samsung-tv-without-re-encoding.2341579/#post-28888556
# it can be tricked to actually play those files ... yes it's intentional just to limit users, it can play it with different codec id
#


# F=file.to.convert.avi
# ffmpeg -y -i "$F" -c copy -bsf:v mpeg4_unpack_bframes -vtag FMP4 "/tmp/new.avi" && mv /tmp/new.avi "$F
# aviIdentify() { COD="$(ffprobe -v error -select_streams v:0 -show_entries 'stream=codec_name,codec_tag_string' -of default=noprint_wrappers=1:nokey=1 "$1")"; echo $COD; }

PRETEND=no
if [[ "$1" == "--pretend" ]]; then
    PRETEND=yes
    export PRETEND
    shift
fi

fix_avi() {
    local src codec tmpCodec tmpFile output

    src="$1"
    # identify
    codec="$(ffmpeg -i "$src" |& sed -nr "s|^\s*Stream.*Video:.*\(([^\)]+ / [^\)]+)\), .*$|\1|p")"
    tmpCodec="-bsf:v mpeg4_unpack_bframes -vtag FMP4"

    echo -n "$codec  in  $src"
    if [[ "$codec" =~ ^XVID || "$codec" =~ ^xvid || "$codec" =~ ^DX50 || "$codec" =~ ^DIV3 ]]; then
        if [[ "$codec" =~ ^DIV3 ]]; then
            tmpCodec="-vtag MP43"
        fi
        echo -n ": "
        if [[ "$PRETEND" == "yes" ]]; then
            echo "pretending only" # terminate line, instead of printing out time ffmpeg took
            return # just detect but do not modify anything
        fi
    else
        echo "" # terminate line, we are skipping this file
       return
    fi

    tmpFile="$(mktemp --suffix ".avi" -p "$(dirname "$src")")"

    export TIMEFORMAT="%lR"
    output="$(time (ffmpeg -i "$src" -c copy $tmpCodec -y "$tmpFile" 2>&1))"
    if [[ "$?" == 0 ]]; then
        mv "$tmpFile" "$src"
    else
        rm -f "$tmpFile"
        echo "$output"
    fi

}

if [[ -d "$1" ]]; then
    export -f fix_avi
    find "$1" -iname \*.avi -print0 | xargs -0 -r -n 1 -I {} bash -c 'fix_avi "$@"' fix_avi {}
else
    while [[ "$#" -gt 0 ]]; do
        fix_avi "$1"
        shift
    done
fi
