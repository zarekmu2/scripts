#!/usr/bin/env python3

# time-log-diff: print time diff between each line in log file
#
# TLDR Example:
# time-log-diff.py <(journalctl -o short-precise -u nginx|tail -n4|cut -c -60)
#
# Provided log file with lines in format as:
#
#     Apr 10 17:15:33.089402 qs-carbon systemd[1]: Starting The ng...
#     Apr 10 17:15:33.907964 qs-carbon nginx[458605]: nginx: the c...
#     Apr 10 17:15:33.908474 qs-carbon nginx[458605]: nginx: confi...
#     Apr 10 17:15:33.932259 qs-carbon systemd[1]: Started The ngi...
#
# It can produce output with milisecond time account for each task,
# until next one appeared:
#
#     818562  Apr 10 17:15:33.089402 qs-carbon systemd[1]: Starting The ng.
#     510     Apr 10 17:15:33.907964 qs-carbon nginx[458605]: nginx: the c...
#     23785   Apr 10 17:15:33.908474 qs-carbon nginx[458605]: nginx: confi.
#     0       Apr 10 17:15:33.932259 qs-carbon systemd[1]: Started The ngi...
#
# Which is then useful for identifying the slowest individual events.
#
# Note:
#  - sort the output of this tool by number for example ;)
#  - to get most of it you may want to apply some filtering
#    ... and thinking about preparing your input log file
#  - account to current line until time-of-next may shift the times by one
#    line obviously
#    - if this is bad or good depends on each software
#      and what/how/howmuch it logs
#    - some sw [my case where i wanted this] logs when going to do work
#    - while other sw logs after work is done (quite often webservers/proxies)
#    In any case this tool can just help identify individual lines
#    and/or section where problem may be, rest is up to you.

from __future__ import print_function

from datetime import datetime, timedelta
import re
import sys

TIME_FORMAT = '%b %d %H:%M:%S.%f'
SPLIT_FORMAT = re.compile('^([a-zA-Z]{3} [0-9]{2} [0-9:]{8}\\.[0-9]+).*')
ONE_MS = timedelta(microseconds=1)

if len(sys.argv) < 2 or '--help' in sys.argv:
    print('''Specify one parameter - path to file with timestamps.
          (like from `journalctl -o short-precise ...`)''')

prevTime = None
prevLine = None
dt = None
with open(sys.argv[1]) as inp:
    for lnFull in inp.readlines():
        if (SPLIT_FORMAT is None):
            lnDate = lnFull
        else:
            m = SPLIT_FORMAT.match(lnFull)
            if (m is None):
                print('ERROR: line not matching format: %s' % (lnFull,),
                      file=sys.stderr)
                continue
            lnDate = m.group(1)
        dt = datetime.strptime(lnDate, TIME_FORMAT)
        if (prevTime is not None):
            print('%d\t%s' % (((dt - prevTime) / ONE_MS), prevLine.strip()))
        prevTime = dt
        prevLine = lnFull
    print('%d\t%s' % (((dt - prevTime) / ONE_MS), prevLine))
